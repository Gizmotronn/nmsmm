import { Component, ViewChild, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { ISettings, SettingsService } from '../services/settings/settings.service';
import { ModsService, IActiveMod, IAvailableMod } from '../services/mods/mods.service';
import { MatTable } from '@angular/material/table';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { AppComponent } from '../app.component';
import { Mod } from '../shared/classes/mod.class';
import { MatDialog } from '@angular/material/dialog';
import { ConfigComponent } from '../dialogs/config/config.component';
import { LoggerService } from '../services/logger/logger.service';
import { UtilitiesService } from '../services/utilities/utilities.service';

@Component({
    selector: 'app-mods',
    templateUrl: './mods.component.html',
    styleUrls: ['./mods.component.scss'],
})
export class ModsComponent implements AfterViewChecked
{
    @ViewChild('activeModsTable', { static: false })
    private _activeModsTable: MatTable<IActiveMod>;

    @ViewChild('availableModsTable', { static: false })
    private _availableModsTable: MatTable<IAvailableMod>;

    private _settings: ISettings;

    public activeMods: IActiveMod[] = [];
    public availableMods: Mod[] = [];
    public ToFixedLength = this._utils.ToFixedLength;

    private _init: boolean = false;
    private _firstTimeViewChecked = true;

    constructor(
        private _settingsService: SettingsService,
        private _modsService: ModsService,
        private _appComponent: AppComponent,
        private _changeDetector: ChangeDetectorRef,
        private _logger: LoggerService,
        private _utils: UtilitiesService,
        public dialog: MatDialog,
    )
    {
        this._init = true;

        this._settingsService.settings.subscribe((data: ISettings) =>
        {
            this._settings = data;
        });

        this._modsService.activeModsUpdate.subscribe((data: IActiveMod[]) =>
        {
            this.activeMods = data;

            this._renderRows();
        });

        this._modsService.availableModsUpdate.subscribe((data: Mod[]) =>
        {
            this.availableMods = data || [];

            this._renderRows();
        });
    }

    public ngAfterViewChecked()
    {
        if(this._firstTimeViewChecked)
        {
            this._changeDetector.detectChanges();
            this._firstTimeViewChecked = false;
        }
    }

    private _renderRows(): void
    {
        if(this._activeModsTable !== undefined)
        {
            this._activeModsTable.renderRows();
        }

        if(this._availableModsTable !== undefined)
        {
            this._availableModsTable.renderRows();
        }

        if(this._activeModsTable !== undefined && this._availableModsTable !== undefined)
        {
            this._changeDetector.detectChanges();
        }
    }

    public trackBy(index: number, item: IActiveMod | IAvailableMod): any
    {
        return index;
    }

    public get init(): boolean
    {
        return this._init;
    }

    public get activeModsColumns(): string[]
    {
        return ['order', 'filename', 'info'];
    }

    public get availableModsColumns(): string[]
    {
        return ['filename', 'info'];
    }

    public get settings(): ISettings
    {
        return this._settings;
    }

    public get noPath(): boolean
    {
        return this._settings === undefined || this._settings.path === undefined || this._settings.path.trim() === '';
    }

    public DroppedModActive(event: CdkDragDrop<Mod[]>): void
    {
        const self = this.DroppedMod(event);

        if(!self)
        {
            const dialogRef = this.dialog.open(ConfigComponent, {
                autoFocus: false,
                disableClose: true,
                width: '90%',

                data: {
                    mod: event.container.data[event.currentIndex],
                },
            });

            dialogRef.afterClosed()
                .subscribe((result) =>
                {
                    if(result === undefined || !result)
                    {
                        transferArrayItem(event.container.data, event.previousContainer.data, event.currentIndex, event.previousIndex);

                        this._renderRows();
                    }
                });
        }
    }

    public DroppedModAvailable(event: CdkDragDrop<Mod[]>): void
    {
        this.DroppedMod(event);
    }

    public DroppedMod(event: CdkDragDrop<Mod[]>): boolean
    {
        let self: boolean;

        if(event.previousContainer === event.container)
        {
            self = true;

            if(!event.container.sortingDisabled)
            {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            }
        }
        else
        {
            self = false;

            transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
        }

        this.availableMods = this._modsService.Sort(this.availableMods);

        this._renderRows();

        return self;
    }

    public OpenInfo(mod: Mod, e: Event): void
    {
        e.preventDefault();
        e.stopPropagation();

        this._appComponent.OpenInfo(mod);
    }
}
