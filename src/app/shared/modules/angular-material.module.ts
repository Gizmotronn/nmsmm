import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';

// CDK
import { DragDropModule } from '@angular/cdk/drag-drop';

const modules: any[] = [
    // Components
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    MatStepperModule,
    MatSelectModule,
    MatTabsModule,

    // CDK
    DragDropModule,
];

@NgModule({
    imports: [
        CommonModule,
    ].concat(modules),
    exports: modules,
    declarations: [],
})
export class MaterialModule { }
