import { Injectable, Output } from '@angular/core';
import { PCBANKS_SEGMENTS, AVAILABLE_DIR } from '../../shared/constants';

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { SettingsService, ISettings, fsReadFileOptions } from '../settings/settings.service';
import { ElectronService } from '../../core/services';

import * as path from 'path';
import { W_OK } from 'constants';

import { Dirent } from 'fs';
import { Mod } from '../../shared/classes/mod.class';
import { ModFactoryService } from '../mod-factory/mod-factory.service';
import { LoggerService } from '../logger/logger.service';

export interface IAvailableMod
{
    filename: string;
}

export interface IActiveMod extends IAvailableMod
{
    order: number;
}

@Injectable({
    providedIn: 'root',
})
export class ModsService
{
    @Output() activeModsUpdate: BehaviorSubject<IActiveMod[]> = new BehaviorSubject<IActiveMod[]>([]);
    @Output() availableModsUpdate: BehaviorSubject<Mod[]> = new BehaviorSubject<Mod[]>([]);

    private _fileOptions: fsReadFileOptions = { encoding: 'utf8', flag: 'a+' };
    private _gamePath: string;

    private _availableMods: Mod[];
    private _activeMods: IActiveMod[];

    private _watching: boolean = false;

    constructor(
        private _settingsService: SettingsService,
        private _electronService: ElectronService,
        private _modFactory: ModFactoryService,
        private _logger: LoggerService,
    )
    {
        this._GetAvailableMods();

        this._settingsService.settings.subscribe((data: ISettings) =>
        {
            if(data === undefined || data.path === undefined) { return null; }

            const newPath = path.join(data.path, ...PCBANKS_SEGMENTS);

            if(newPath !== this._gamePath)
            {
                this._gamePath = newPath;

                this._GetActiveMods();
            }
        });
    }

    private _GetActiveMods(): void
    {
        this._electronService.fs.readdir(path.join(this._gamePath, 'MODS'), this._fileOptions)
            .then((contents: string[]) =>
            {
                this._activeMods = this.SortLegacy(contents
                    .filter((value: string) =>
                    {
                        return value.endsWith('.pak');
                    })
                    .map((value: string) =>
                    {
                        return {
                            filename: value
                                .replace('.pak', ''),
                        };
                    }))
                    .map((value: IAvailableMod, index: number) =>
                    {
                        (value as IActiveMod).order = index + 1;

                        return value;
                    }) as IActiveMod[];

                this.activeModsUpdate.next(this._activeMods);
            })
            .catch((e) =>
            {
                this._logger.error(e);
            });
    }

    private _GetAvailableMods(): void
    {
        this._electronService.fs.access(AVAILABLE_DIR, W_OK)
            .then(async () =>
            {
                if(!this._watching)
                {
                    this._SetupDirWatch();
                }

                return this._electronService.fs.readdir(AVAILABLE_DIR, {
                    encoding: 'utf8',
                    withFileTypes: true,
                });
            })
            .then(async (contents: Dirent[]) =>
            {
                return Promise.resolve(contents.filter((item: Dirent) =>
                {
                    return item.isDirectory();
                }));
            })
            .then((contents: Dirent[]) =>
            {
                this._availableMods = this.Sort(contents.map((item: Dirent) =>
                {
                    return this._modFactory.Create(item);
                }));

                this.availableModsUpdate.next(this._availableMods);
            })
            .catch(async (accessErr) =>
            {
                if(accessErr.code === 'ENOENT')
                {
                    this._CreateAvailableModsDir()
                        .then(() =>
                        {
                            this._logger.log('Created available mods directory');
                        })
                        .catch((createErr) =>
                        {
                            this._logger.error(createErr);
                        });
                }
                else
                {
                    this._logger.error(accessErr);
                }
            });
    }

    private async _CreateAvailableModsDir(): Promise<void>
    {
        return this._electronService.fs.mkdir(AVAILABLE_DIR, { recursive: true });
    }

    private _SetupDirWatch(): void
    {
        this._watching = true;

        this._electronService.watch(AVAILABLE_DIR, { ignoreInitial: true })
            .on('add', (event: string, filePath: string) =>
            {
                this._GetAvailableMods();
            });
    }

    public Sort(items: Mod[]): Mod[]
    {
        return items.sort((a: Mod, b: Mod) =>
        {
            return a.name.localeCompare(b.name);
        });
    }

    public SortLegacy<T extends IAvailableMod>(items: T[]): T[]
    {
        return items.sort((a: T, b: T) =>
        {
            return a.filename.localeCompare(b.filename);
        });
    }
}
