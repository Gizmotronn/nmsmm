import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class LoggerService
{
    constructor()
    {

    }

    public log(...message: any[]): void
    {
        console.log('NMSMM |', ...message);
    }

    public error(...message: any[]): void
    {
        console.error('NMSMM |', ...message);
    }

    public debug(...message: any[]): void
    {
        if(!process.env.production)
        {
            console.debug('NMSMM |', ...message); // tslint:disable-line no-console
        }
    }
}
