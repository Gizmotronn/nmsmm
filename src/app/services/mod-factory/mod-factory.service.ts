import { Injectable } from '@angular/core';

import { ElectronService } from '../../core/services';
import { Dirent } from 'fs';
import { Mod } from '../../shared/classes/mod.class';
import { LoggerService } from '../logger/logger.service';

@Injectable({
    providedIn: 'root',
})
export class ModFactoryService
{
    constructor(
        private _electron: ElectronService,
        private _logger: LoggerService,
    )
    {

    }

    public Create(directory: Dirent): Mod
    {
        return new Mod(this._electron, this._logger, directory);
    }
}
