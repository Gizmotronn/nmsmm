import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './shared/components';

import { ModsComponent } from './mods/mods.component';
import { SettingsComponent } from './settings/settings.component';
import { UtilitiesComponent } from './utilities/utilities.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'mods',
        pathMatch: 'full',
    },
    {
        path: 'mods',
        component: ModsComponent,
        pathMatch: 'full',
    },
    {
        path: 'utilities',
        component: UtilitiesComponent,
        pathMatch: 'full',
    },
    {
        path: 'settings',
        component: SettingsComponent,
        pathMatch: 'full',
    },
    {
        path: '**',
        component: PageNotFoundComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule],
})
export class AppRoutingModule { }
