import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { WebviewDirective } from './directives/';

import { PageNotFoundComponent } from './components/';

@NgModule({
    declarations: [
        WebviewDirective,
        PageNotFoundComponent,
    ],
    imports: [
        CommonModule,
        TranslateModule,
    ],
    exports: [
        TranslateModule,
        WebviewDirective,
    ],
})
export class SharedModule { }
