import { Component } from '@angular/core';

import { OpenDialogReturnValue } from 'electron';

import { SettingsService, ISettings, ILanguage } from '../services/settings/settings.service';
import { ElectronService } from '../core/services';
import { LoggerService } from '../services/logger/logger.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent
{
    private _settings: ISettings = {} as any;

    private _selectedLanguage: ILanguage;

    constructor(
        private _settingsService: SettingsService,
        private _electron: ElectronService,
        private _logger: LoggerService,
    )
    {
        this._settingsService.settings.subscribe((settings: ISettings) =>
        {
            if(settings !== undefined)
            {
                this._settings = settings;

                if(this._selectedLanguage === undefined)
                {
                    this._selectedLanguage = this._settingsService.GetLanguage(this._settings.language);
                }
            }
        });
    }

    public SaveSettings(): void
    {
        this._settingsService.SaveSettings(this._settings);
    }

    public get settingsLoaded(): boolean
    {
        return this._settings !== undefined;
    }

    public PickFolder()
    {
        this._electron.remote.dialog.showOpenDialog({ properties: ['openDirectory'] })
            .then((data: OpenDialogReturnValue) =>
            {
                if(data.filePaths.length > 0)
                {
                    this._settings.path = data.filePaths[0];

                    this.SaveSettings();
                }
            })
            .catch((e) =>
            {
                this._logger.error(`It broke: ${e}`);
            });
    }

    public get selectedLanguage(): ILanguage
    {
        return this._selectedLanguage;
    }

    public set selectedLanguage(language: ILanguage)
    {
        this._selectedLanguage = language;
        this._settings.language = this._selectedLanguage.code;

        this.SaveSettings();
    }

    public languageTrackBy(language: ILanguage): string
    {
        return language.code;
    }

    public get settings(): ISettings
    {
        return this._settings;
    }

    public get languages(): ILanguage[]
    {
        return this._settingsService.languages;
    }
}
