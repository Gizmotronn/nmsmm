import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class UtilitiesService
{
    constructor()
    {

    }

    public ToFixedLength(num: number, length: number): string
    {
        let r = '' + num;

        while(r.length < length)
        {
            r = '0' + r;
        }

        return r;
    }
}
