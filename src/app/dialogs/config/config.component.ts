import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Mod, IModConfigOption, IModConfig } from '../../shared/classes/mod.class';
import { LoggerService } from '../../services/logger/logger.service';
import { UtilitiesService } from '../../services/utilities/utilities.service';

interface IStepForms
{
    [key: string]: IStepForm;
}

interface IStepForm
{
    option?: string;
    options?: IStepOption;
}

interface IStepOption
{
    [key: string]: boolean;
}

@Component({
    selector: 'app-config',
    templateUrl: './config.component.html',
    styleUrls: ['./config.component.scss'],
})
export class ConfigComponent
{
    private _mod: Mod;
    private readonly _maxOptionsPerRow: number = 5;

    public stepForms: IStepForms = {};
    public selectedIndex = 0;
    public ToFixedLength = this._utils.ToFixedLength;

    constructor(
        public dialogRef: MatDialogRef<ConfigComponent>,
        private _utils: UtilitiesService,
        private _logger: LoggerService,
        @Inject(MAT_DIALOG_DATA) public data: { mod: Mod },
    )
    {
        this._mod = data.mod;

        for(let i = 0; i < this.configs.length; i++)
        {
            const config = this.configs[i];

            this.stepForms[config.id] = {};

            if(config.multi)
            {
                this.stepForms[config.id].options = {};
            }
        }
    }

    public Previous(): void
    {
        let nextIndex = this.selectedIndex - 1;

        while(!this.RequirementMet(this.configs[nextIndex]))
        {
            nextIndex--;

            if(nextIndex < 0) { return null; }
        }

        this.selectedIndex = nextIndex;
    }

    public Next(): void
    {
        let nextIndex = this.selectedIndex + 1;

        while(!this.RequirementMet(this.configs[nextIndex]))
        {
            nextIndex++;

            if(nextIndex > this.configs.length) { return null; }
        }

        this.selectedIndex = nextIndex;
    }

    public IsComplete(config: IModConfig): boolean
    {
        if(config.multi)
        {
            return true;
        }

        if(config.options)
        {
            return Object.keys(config.options)
                .includes(this.stepForms[config.id].option);
        }

        return true;
    }

    public RequirementMet(config: IModConfig): boolean
    {
        if(config === undefined || config.requires === undefined) { return true; }

        const segments: string[] = config.requires.split('.');

        const requirement = this._mod.config[segments[0]];

        if(requirement === undefined) { return true; }

        const requirementForm = this.stepForms[requirement.id];

        return requirement.multi ? requirementForm.options[segments[1]] : requirementForm.option === segments[1];
    }

    public get toBeInstalled(): string[]
    {
        let files: string[] = [];
        files = files.concat(this._mod.files);

        const configKeys = Object.keys(this.stepForms);

        for(let iConfig = 0; iConfig < configKeys.length; iConfig++)
        {
            const configKey = configKeys[iConfig];
            const config = this._mod.config[configKey];
            const option = this.stepForms[configKey];

            if(config === undefined) { continue; }
            if(!this.RequirementMet(config)) { continue; }

            if(config.multi)
            {
                const optionKeys = Object.keys(option.options);

                for(let iOption = 0; iOption < optionKeys.length; iOption++)
                {
                    const optionKey = optionKeys[iOption];
                    const optionSelected = option.options[optionKey];

                    if(optionSelected)
                    {
                        files = files.concat(config.options[optionKey].files);
                    }
                }
            }
            else
            {
                if(config.options[option.option] === undefined) { continue; }

                files = files.concat(config.options[option.option].files);
            }
        }

        return files.filter((item: string, index: number) =>
        {
            return files.indexOf(item) === index;
        });
    }

    public get maxOptionsPerRow(): number
    {
        return this._maxOptionsPerRow;
    }

    public get mod(): Mod
    {
        return this._mod;
    }

    public get configs(): IModConfig[]
    {
        return Object.values(this._mod.config);
    }

    public options(config: IModConfig): IModConfigOption[]
    {
        return Object.values(config.options);
    }

    public Install(): void
    {
        console.log(this.stepForms);
    }
}
