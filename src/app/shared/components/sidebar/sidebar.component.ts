import { Component, Input } from '@angular/core';
import { Icon } from '../../modules/font-awesome.module';
import { ElectronService } from '../../../core/services';

interface ILink
{
    ref?: string;
    func?: Function;
    textID: string;
    icon: Icon;
}

interface ILinks
{
    top: ILink[];
    bottom: ILink[];
}

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent
{
    public links: ILinks = {
        top: [
            {
                ref: '/mods',
                textID: 'mods',
                icon: 'tools',
            },
            {
                ref: '/utilities',
                textID: 'utilities',
                icon: 'toolbox',
            },
        ],
        bottom: [
            {
                ref: '/settings',
                textID: 'settings',
                icon: 'cog',
            },
            {
                func: this.Quit,
                textID: 'quit',
                icon: 'door-open',
            },
        ],
    };

    constructor(
        private _electron: ElectronService,
    )
    {

    }

    public Quit()
    {
        this._electron.Close();
    }
}
