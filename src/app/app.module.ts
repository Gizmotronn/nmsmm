import 'reflect-metadata';
import '../polyfills';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './shared/modules/angular-material.module';
import { FontAwesomeModule as App_FontAwesomeModule } from './shared/modules/font-awesome.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';

import { ModsComponent } from './mods/mods.component';
import { SettingsComponent } from './settings/settings.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UtilitiesComponent } from './utilities/utilities.component';
import { ConfigComponent } from './dialogs/config/config.component';

// AoT requires an exported function for factories
export const HttpLoaderFactory = (http: HttpClient) =>
{
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    declarations: [
        AppComponent,
        SidebarComponent,
        ModsComponent,
        SettingsComponent,
        UtilitiesComponent,
        ConfigComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        CoreModule,
        SharedModule,
        FontAwesomeModule,
        AppRoutingModule,
        MaterialModule,
        App_FontAwesomeModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        BrowserAnimationsModule,
    ],
    entryComponents: [
        ConfigComponent,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule { }
