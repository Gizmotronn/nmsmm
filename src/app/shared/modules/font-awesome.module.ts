import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';

export type Icon = string | [string, string];

// Solid icons
import
{
    IconDefinition,
    faTools,
    faCog,
    faDoorOpen,
    faFolderOpen,
    faToolbox,
    faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';

// Regular icons
import
{
} from '@fortawesome/free-regular-svg-icons';

const modules: IconDefinition[] = [
    faTools,
    faCog,
    faDoorOpen,
    faFolderOpen,
    faToolbox,
    faInfoCircle,
];

@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [],
    declarations: [],
})
export class FontAwesomeModule
{
    constructor(library: FaIconLibrary)
    {
        for(let i = 0; i < modules.length; i++)
        {
            library.addIcons(modules[i]);
        }
    }
}

