import { join } from 'path';

const remote = window && window.process && window.process.type ? window.require('electron').remote : undefined;

export const PCBANKS_SEGMENTS: string[] = ['GAMEDATA', 'PCBANKS'];
export const AVAILABLE_MODS_SEGMENTS: string[] = ['mods'];
export const LOCAL_DIR: string = remote.app.getAppPath();
export const AVAILABLE_DIR: string = join(LOCAL_DIR, ...AVAILABLE_MODS_SEGMENTS);
