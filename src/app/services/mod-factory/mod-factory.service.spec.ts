import { TestBed } from '@angular/core/testing';

import { ModFactoryService } from './mod-factory.service';

describe('ModFactoryService', () =>
{
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () =>
    {
        const service: ModFactoryService = TestBed.get(ModFactoryService);
        expect(service).toBeTruthy();
    });
});
