import * as path from 'path';

import { parse } from 'yaml';

import { Dirent } from 'fs';
import { ElectronService } from '../../core/services';
import { AVAILABLE_DIR } from '../constants';
import { LoggerService } from '../../services/logger/logger.service';

import { SemVer } from 'semver';

export interface IModInfo
{
    name: string;
    version: SemVer;
    author?: string;
    description?: string;
    files?: string[];
    config?: IModConfigs;
}

export interface IModConfigs
{
    [key: string]: IModConfig;
}

export interface IModConfig
{
    id: string;
    title: string;
    description: string;
    requires?: string;
    options: IModConfigOptions;
    multi?: boolean;
}

export interface IModConfigOptions
{
    [key: string]: IModConfigOption;
}

export interface IModConfigOption
{
    id: string;
    title: string;
    description?: string;
    files: string[];
}

export class Mod
{
    private _order: number;
    private _active: boolean = false;
    private _filePath: string;
    private _info: IModInfo;

    public hasModFile: boolean;

    constructor(
        private _electron: ElectronService,
        private _logger: LoggerService,
        private _directory: Dirent,
    )
    {
        this._filePath = path.join(AVAILABLE_DIR, this._directory.name, 'mod.yml');

        this.GetModInfo();
    }

    private GetModInfo(): void
    {
        this._electron.fs.readFile(this._filePath, { encoding: 'utf8', flag: 'r' })
            .then((fileContents: string) =>
            {
                const parsed = parse(fileContents);
                parsed.version = new SemVer(parsed.version);

                this._info = parsed;

                const configKeys = Object.keys(this._info.config);

                for(let iConfig = 0; iConfig < configKeys.length; iConfig++)
                {
                    const configKey = configKeys[iConfig];
                    const config = this._info.config[configKey];

                    config.id = configKey;

                    if(config.options !== undefined)
                    {
                        const optionKeys = Object.keys(config.options);

                        for(let iOption = 0; iOption < optionKeys.length; iOption++)
                        {
                            const optionKey = optionKeys[iOption];
                            const option = config.options[optionKey];

                            option.id = optionKey;
                        }
                    }
                }

                this._logger.debug(this._info);

                this.hasModFile = true;
            })
            .catch((e) =>
            {
                if(e.code === 'ENOENT')
                {
                    this._info = {
                        name: this._directory.name,
                        version: new SemVer('0.0.0'),
                    };

                    this.hasModFile = false;
                }
                else
                {
                    this._logger.error(e);
                }
            });
    }

    public get name(): string
    {
        return this._info !== undefined ? this._info.name || '' : '';
    }

    public get version(): string
    {
        return (this._info !== undefined && this._info.version !== undefined) ? this._info.version.version : '';
    }

    public get author(): string
    {
        return this._info !== undefined ? this._info.author || '' : '';
    }

    public get description(): string
    {
        return this._info !== undefined ? this._info.description || '' : '';
    }

    public get loadOrder(): number
    {
        return this._order;
    }

    public set loadOrder(order: number)
    {
        this._order = order;
    }

    public get files(): string[]
    {
        return this._info.files;
    }

    public get config(): IModConfigs
    {
        return this._info.config;
    }
}
