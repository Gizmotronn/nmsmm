import { app, BrowserWindow, screen } from 'electron';

import * as path from 'path';
import * as url from 'url';

const args: string[] = process.argv.slice(1);

let win: BrowserWindow;
let serve: boolean;

serve = args.some((val) => val === '--serve');

const createWindow = () =>
{
    const electronScreen: Electron.Screen = screen;
    const workAreaSize: Electron.Size = electronScreen.getPrimaryDisplay().workAreaSize;

    const size: Electron.Size = {
        width: 1550,
        height: 900,
    };

    const position = {
        x: (workAreaSize.width / 2) - (size.width / 2),
        y: (workAreaSize.height / 2) - (size.height / 2),
    };

    // Create the browser window.
    win = new BrowserWindow({
        x: position.x,
        y: position.y,
        width: size.width,
        height: size.height,
        webPreferences: {
            nodeIntegration: true,
        },
        title: 'No Man\'s Sky Mod Manager',
    });

    win.setMenu(null);

    if(serve)
    {
        require('electron-reload')(__dirname, {
            electron: require(`${__dirname}/node_modules/electron`),
        });

        const pageUrl: string = 'http://localhost:4200/mods';

        win.loadURL(pageUrl)
            .then(() =>
            {
                console.log(`Loaded URL ${pageUrl}`);
            })
            .catch((e) =>
            {
                console.error(`Error loading URL ${pageUrl}: ${e}`);
            });
    }
    else
    {
        const pagePath: string = path.join(__dirname, 'dist/index.html');

        win.loadURL(url.format({
            pathname: pagePath,
            protocol: 'file:',
            slashes: true,
        }))
            .then(() =>
            {
                console.log(`Loaded path ${pagePath}`);
            })
            .catch((e) =>
            {
                console.error(`Error loading path ${pagePath}: ${e}`);
            });
    }

    if(serve)
    {
        win.webContents.openDevTools();
    }

    // Emitted when the window is closed.
    win.on('closed', () =>
    {
        // Dereference the window object, usually you would store window
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });
};

try
{
    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    app.on('ready', createWindow);

    // Quit when all windows are closed.
    app.on('window-all-closed', () =>
    {
        // On OS X it is common for applications and their menu bar
        // to stay active until the user quits explicitly with Cmd + Q
        if(process.platform !== 'darwin')
        {
            app.quit();
        }
    });

    app.on('activate', () =>
    {
        // On OS X it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if(win === null)
        {
            createWindow();
        }
    });

}
catch(e)
{
    // Catch Error
    throw e;
}
