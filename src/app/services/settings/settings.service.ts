import { Injectable, Output } from '@angular/core';
import { ElectronService } from '../../core/services';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import * as path from 'path';
import { LoggerService } from '../logger/logger.service';

export interface ISettings
{
    path: string;
    language: string;
}

export interface ILanguage
{
    text: string;
    code: string;
    incomplete?: boolean;
}

export type fsReadFileOptions = string | {
    encoding: string;
    mode?: string | number;
    flag?: string | number;
};

export type fsReadDirOptions = string | {
    encoding: string;
    withFileTypes?: boolean;
};

@Injectable({
    providedIn: 'root',
})
export class SettingsService
{
    @Output() settings: BehaviorSubject<ISettings> = new BehaviorSubject<ISettings>(undefined);

    private _filePath: string[] = ['settings.json'];
    private _fileOptions: fsReadFileOptions = { encoding: 'utf8', flag: 'a+' };
    private _settings: ISettings;

    private _path: string;

    public languages: ILanguage[] = [
        {
            text: 'English',
            code: 'en',
        },
        {
            text: 'Español',
            code: 'es',
            incomplete: true,
        },
    ];

    constructor(
        private _electron: ElectronService,
        private _logger: LoggerService,
    )
    {
        this.settings = new BehaviorSubject<ISettings>(this._settings);

        this._path = path.join(process.env.INIT_CWD || process.env.PORTABLE_EXECUTABLE_DIR, ...this._filePath);

        this._LoadSettings();
    }

    private _LoadSettings()
    {
        this._logger.log('Loading settings');

        this._electron.fs.readFile(this._path, this._fileOptions)
            .then((content: string) =>
            {
                this._logger.log('Read settings file');

                try
                {
                    this._settings = JSON.parse(content) as ISettings;
                }
                catch(e)
                {
                    this._settings = {} as any;
                }

                this._settings.language = this._settings.language || this.languages[0].code;

                this.SaveSettings(this._settings);

                this.settings.next(this._settings);
            })
            .catch((e) =>
            {
                if(e.code === 'ENOENT')
                {
                    this._logger.log('No settings file found. Creating one');

                    this._CreateSettingsFile();
                }
            });
    }

    private _CreateSettingsFile()
    {
        const data = {};

        this._electron.fs.writeFile(this._path, JSON.stringify(data), this._fileOptions)
            .then(() =>
            {
                this._logger.log('Created settings file');

                this._settings = data as ISettings;

                this.settings.next(this._settings);
            })
            .catch((e) =>
            {
                this._logger.error(`Error creating settings file: ${e.message}`);
            });
    }

    public GetSettings()
    {
        return Object.assign({}, this._settings);
    }

    public SaveSettings(settings: ISettings)
    {
        this._electron.fs.writeFile(this._path, JSON.stringify(settings), Object.assign({}, this._fileOptions, { flag: 'w' }))
            .then(() =>
            {
                this.settings.next(this._settings);
            })
            .catch((e) =>
            {
                this._logger.error('Error saving settings', e);
            });
    }

    public GetLanguage(code: string): ILanguage
    {
        return this.languages.find((language: ILanguage) =>
        {
            return language.code === code;
        });
    }
}
