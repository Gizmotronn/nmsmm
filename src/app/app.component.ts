import { Component } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { Router } from '@angular/router';
import { Mod } from './shared/classes/mod.class';
import { SettingsService, ISettings } from './services/settings/settings.service';
import { LoggerService } from './services/logger/logger.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent
{
    public infoMod: Mod;
    public infoOpen: boolean = false;

    constructor(
        public electronService: ElectronService,
        private _translate: TranslateService,
        private _router: Router,
        private _settingsService: SettingsService,
        private _logger: LoggerService,
    )
    {
        this._logger.debug('AppConfig:', AppConfig);

        if(this.electronService.isElectron)
        {
            this._logger.debug('process.env:', process.env);
            this._logger.debug('Mode: electron');
        }
        else
        {
            this._logger.debug('Mode: web');
        }

        this._translate.setDefaultLang('en');

        this._settingsService.settings.subscribe((settings: ISettings) =>
        {
            this._translate.use(settings !== undefined ? settings.language : this._translate.currentLang);
        });
    }

    public get route(): string
    {
        return this._router.url.replace('/', '');
    }

    public OpenInfo(mod: Mod): void
    {
        this.infoMod = mod;

        this.infoOpen = true;
    }
}
